import { Component, OnInit } from "@angular/core";
// import {R} from '@angular/router';

@Component({
  selector: "main-component",
  template: `<h2 style="padding:10px;">Home</h2>
    <session-timeout
      [countDownSeconds]="countDownSecondsValue"
      [idleTimeoutSeconds]="idleTimeoutSecondsValue"
    ></session-timeout>
    <div
      style="display:flex;flex-direction:column;width:35em;height:10em;padding:10px;justify-content:space-evenly;"
    >
      <div
        style="display:flex;flex-direction:row;justify-content:space-between;"
      >
        <mat-form-field class="example-full-width" appearance="fill">
          <mat-label>Countdown Timer</mat-label>
          <input
            matInput
            type="number"
            placeholder="Countdown Timer"
            [(ngModel)]="countDownSecondsValue"
          />
        </mat-form-field>
        <mat-form-field class="example-full-width" appearance="fill">
          <mat-label>Dialog Appear Timer</mat-label>
          <input
            matInput
            type="number"
            placeholder="Dialog Appear Timer"
            [(ngModel)]="idleTimeoutSecondsValue"
          />
        </mat-form-field>
      </div>
      <div
        style="display:flex; flex-direction:row;justify-content:space-between;width:20em;"
      >
        <a mat-raised-button style="background-color:#3c3ca7;" target="_blank" routerLink="/"
          >Open New tab</a
        >
      </div>
    </div> `,
})
export class MainComponent implements OnInit {
  public countDownSecondsValue: number = 20;
  public idleTimeoutSecondsValue: number = 10;
  constructor() {}

  ngOnInit() {}

  //   public openTab():void{
  //       const url:string=this.rou
  //   }
}
