import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { LoggedOutComponent } from "./logged-out/logged-out.component";
import { MainComponent } from "./main/main.component";

import { MatButtonModule } from "@angular/material/button";
import { MatInputModule } from "@angular/material/input";
import { MatDialogModule } from "@angular/material/dialog";
import { FormsModule } from "@angular/forms";

import {
  SessionTimeoutComponent,
  SessionTimeoutPopupComponent,
} from "./session-timeout/session-timeout.component";

const routes: Routes = [
  { path: "", component: MainComponent },
  { path: "loggedOut", component: LoggedOutComponent },
  { path: "**", component: MainComponent },
];

@NgModule({
  declarations: [
    MainComponent,
    LoggedOutComponent,
    SessionTimeoutComponent,
    SessionTimeoutPopupComponent,
  ],
  imports: [
    RouterModule.forRoot(routes),
    MatButtonModule,
    MatInputModule,
    MatDialogModule,
    FormsModule,
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
