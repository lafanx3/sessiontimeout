import {
  Component,
  Inject,
  AfterViewInit,
  Input,
  OnChanges,
  NgZone,
  OnInit,
} from "@angular/core";

import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from "@angular/material/dialog";
import { Router } from "@angular/router";

import { Subject, fromEvent, merge, timer } from "rxjs";

import {
  takeWhile,
  tap,
  takeUntil,
  repeatWhen,
  filter,
  debounceTime,
} from "rxjs/operators";

@Component({
  selector: "session-timeout",
  template: "",
})
export class SessionTimeoutComponent implements AfterViewInit, OnChanges {
  private readonly startTimer: Subject<void> = new Subject<void>();
  private readonly stopTimer: Subject<void> = new Subject<void>();

  private timeoutID: any;
  private dialogRef: MatDialogRef<SessionTimeoutPopupComponent>;

  private _countDownSeconds: number = 59;
  private defaultCountDownSeconds: number;

  private _idleTimeoutSeconds: number = 1800; //30 mins
  private setTimeoutStartTime: number;

  private _broadcastChannel: BroadcastChannel;

  //#region getters/setters

  @Input()
  public set countDownSeconds(value: number) {
    if (value !== undefined) this._countDownSeconds = value;
  }

  public get countDownSeconds(): number {
    return this._countDownSeconds;
  }

  @Input()
  public set idleTimeoutSeconds(value: number) {
    if (value !== undefined) this._idleTimeoutSeconds = value;
  }

  public get broadcastChannel(): BroadcastChannel {
    if (!this._broadcastChannel)
      this._broadcastChannel = new BroadcastChannel("session-timeout");

    return this._broadcastChannel;
  }

  public get idleTimeoutSeconds(): number {
    return this._idleTimeoutSeconds;
  }

  private get dateTimeNowSeconds(): number {
    return Math.floor(Date.now() / 1000);
  }

  private get elapsedTimeSinceSetTimeoutStart(): number {
    let timeLeft: number =
      this.idleTimeoutSeconds -
      (this.dateTimeNowSeconds - this.setTimeoutStartTime);

    if (timeLeft <= 0) timeLeft = this.idleTimeoutSeconds;

    return timeLeft;
  }

  //#endregion

  constructor(
    public dialog: MatDialog,
    private zone: NgZone,
    private router: Router
  ) {}

  ngOnChanges() {
    this.defaultCountDownSeconds = this.countDownSeconds;
  }

  ngAfterViewInit() {
    this.defaultCountDownSeconds = this.countDownSeconds;

    timer(1000, 1000)
      .pipe(
        takeWhile(() => this.countDownSeconds > 0 && !!this.dialogRef),
        tap(() => {
          this.countDownSeconds--;
          this.zone.run(() => {
            this.dialogRef.componentInstance.data = {
              countDownSeconds: this.countDownSeconds,
            };
          });
        }),
        filter(() => this.countDownSeconds == 0),
        takeUntil(this.stopTimer),
        repeatWhen(() => this.startTimer)
      )
      .subscribe(() => {
        this.logout();
      });

    merge(fromEvent(window, "mousemove"), fromEvent(window, "keydown"))
      .pipe(
        debounceTime(1000),
        takeUntil(this.startTimer),
        repeatWhen(() => this.stopTimer)
      )
      .subscribe(() => {
        this.broadcastChannel.postMessage({ resetSetTimeout: true });
        this.startSetTimeout();
      });

    // this.logoutUser.subscribe(() => {
    //   //logout
    // });

    this.broadcastChannel.onmessage = (e: any) => {
      if (e.data.resetSetTimeout) {
        this.startSetTimeout();
      } else if (e.data.extendSessionResponse) {
        if (e.data.extendSessionResponse.extendSession) {
          this.startSetTimeout();
        } else {
          this.logout();
        }
      } else if (e.data.logoutResponse) {
        window.location.href = e.data.logoutResponse.logoutUrl;
      }

      if (this.dialogRef && this.dialogRef.componentInstance) {
        this.zone.run(() => {
          this.dialogRef.close();
        });
        this.stopTimer.next();
      }
    };

    if (this.setTimeoutStartTime == undefined) {
      this.broadcastChannel.postMessage({ resetSetTimeout: true });
      this.startSetTimeout();
    }
  }

  private startSetTimeout(): void {
    clearTimeout(this.timeoutID);
    this.setTimeout();
  }

  private setTimeout(): void {
    this.setTimeoutStartTime = this.dateTimeNowSeconds;

    this.timeoutID = setTimeout(() => {
      this.countDownSeconds = this.defaultCountDownSeconds;
      this.startTimer.next();
      this.zone.run(() => {
        this.dialogRef = this.dialog.open(SessionTimeoutPopupComponent, {
          width: "375px",
          disableClose: true,
          data: { countDownSeconds: this.countDownSeconds },
        });
      });
      this.zone.run(() => {
        this.dialogRef.afterClosed().subscribe((extendSession) => {
          if (extendSession !== undefined) {
            this.broadcastChannel.postMessage({
              extendSessionResponse: { extendSession: extendSession },
            });
            if (extendSession) {
              this.resetSession();
            } else {
              this.logout();
            }
          }
        });
      });
    }, this.elapsedTimeSinceSetTimeoutStart * 1000);
  }

  private resetSession(): void {
    this.stopTimer.next();
    this.startSetTimeout();

    //this.lookupService.authLookup.extendSession();
  }

  private logout(): void {
    clearTimeout(this.timeoutID);
    this.stopTimer.next();
    this.logoutUser();
  }

  public logoutUser(): void {
    //send logout api request
    //subscribe data
    this.broadcastChannel.postMessage({});
    this.router.navigate(["/loggedOut"]);
    //window.location.href = "logout url";
  }

  public extendSession(): void {
    //api request to extend session...
  }

  //#region getters/setters
}

@Component({
  template: `
    <h1 mat-dialog-title>Session Expiring</h1>
    <div mat-dialog-content></div>
    <p>
      Your session is about to expire in {{ countDownTime }}
      {{ data.countDownSeconds > 1 ? "seconds" : "second" }}. Click Continue to
      extend the session.
    </p>
    <div style="display:flex;flex-direction:row;justify-content:flex-end;" mat-dialog-actions>
      <button mat-raised-button color="warn" aria-label="close dialog" mat-dialog-close cdkFocusInitial>Logout</button>
      <button mat-raised-button color="accent" aria-label="close dialog" [mat-dialog-close]="true" cdkFocusInitial>Continue</button>
    </div>
  `,
})
export class SessionTimeoutPopupComponent {
  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}

  public get countDownTime(): string {
    const format = (val: any) => `0${Math.floor(val)}`.slice(-2);

    const hours = this.data.countDownSeconds / 3600;

    const minutes = (this.data.countDownSeconds % 3600) / 60;

    if (Math.floor(hours) > 0)
      return [hours, minutes, this.data.countDownSeconds % 60]
        .map(format)
        .join(":");
    else
      return [minutes, this.data.countDownSeconds % 60].map(format).join(":");
  }
  
}


//TODO
//after page logged out, clicking home on one page will restart timer, howevertimer starts on all previous logged out page and shows session expiring dialog appears on logged out page.
//form fields, timer and countodown, let user control implentation.
//sometimes after page logged out, clicking back to home, still showing previous page(lgged out page) and home page...may need to run ngzone to reset?