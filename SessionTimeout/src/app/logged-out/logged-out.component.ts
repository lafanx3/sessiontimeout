import { Component, OnInit } from "@angular/core";

@Component({
    selector:'logged-out',
    template:`<h2>Logged Out!</h2>
        <button mat-raised-button color="primary" routerLink="/">Home</button> 
        `
})
export class LoggedOutComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
